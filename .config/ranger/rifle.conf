#       _           __    _        __  ___
#      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
#     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
#  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
# |___/    https://gitlab.com/joakimaling    /___/
#
# ~/.config/ranger/rifle.conf
#

# Common
ext [ct]sv|sc|xlsx?, has sc-im = sc-im -- "$@"
ext uxf, has umlet, X, flag f = umlet "$@"
mime ^text, label edit = $EDITOR -- "$@"
mime ^text, label page = $PAGER -- "$@"
!mime ^text, label edit, ext (ba)?sh|c|[ch]pp|css|h|java|json = $EDITOR -- "$@"
!mime ^text, label edit, ext e?js|lua|pas|php|py|s[ac]ss|sql = $EDITOR -- "$@"
!mime ^text, label edit, ext tex|[xy]ml = $EDITOR -- "$@"
!mime ^text, label edit, name ^.(babel|jshint|postcss)rc$ = $EDITOR -- "$@"
!mime ^text, label page, ext (ba)?sh|c|[ch]pp|css|h|java|json = $PAGER -- "$@"
!mime ^text, label page, ext e?js|lua|pas|php|py|s[ac]ss|sql = $PAGER -- "$@"
!mime ^text, label page, ext tex|[xy]ml = $PAGER -- "$@"
!mime ^text, label page, name ^.(babel|jshint|postcss)rc$ = $PAGER -- "$@"
ext pdf, has mupdf, X, flag f = mupdf "$@"
ext pdf, has lp = lp -- "$@"

# Browsing
ext x?html?, has qutebrowser, X, flag f = qutebrowser -- "$@"
ext x?html?, has firefox, X, flag f = firefox -- "$@"

# Coding
name ^[Mm]akefile$ = make
ext (ba)?sh = bash -- "$1"
ext js, has node = node -- "$1"
ext lua, has lua = lua -- "$1"
ext php, has php = php -- "$1"
ext py, has python = python "$1"

# Gaming
ext exe, has dosbox = dosbox "$1" -noconsole
ext exe, has wine = wine "$1"

# Media
ext torrent, has transmission-remote = transmission-remote -a "$@"
ext xcf, has gimp, X, flag f = gimp -- "$@"
ext wav, has paplay = paplay -- "$@"
mime ^audio|video, has mplayer, X, flag t = mplayer -- "$@"
mime ^image, has sxiv, X, flag f = sxiv -a -b "$@"
mime ^image, has feh, X, flag f = feh -B black -. -- "$@"
mime ^image, has gimp, X, flag f = gimp -- "$@"
mime ^image, has lp = lp -- "$@"

# Archives
ext gz|iso|[jrt]ar|xz|zip, has atool = atool -el -- "$@" | $PAGER
ext gz|iso|[jrt]ar|xz|zip, has atool = atool -ex -- "$@"
ext gz|tar|xz, has tar = tar -f "$1" -t | $PAGER
ext gz|tar|xz, has tar = for file in "$@"; do tar -f "$file" -x; done
ext rar, has unrar = unrar l "$1" | $PAGER
ext rar, has unrar = for file in "$@"; do unrar x "$file"; done
ext zip, has unzip = unzip -l "$1" | $PAGER
ext zip, has unzip = for file in "$@"; do unzip -d "${file%.*}" "$file"; done

# Ask if unknown
!mime ^text = ask

# Try to execute
mime application/x-executable|text/x-shellscript = "$1"

# vim:ft=cfg
