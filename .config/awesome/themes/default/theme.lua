-- =============================================================================
--
-- Awesome 4.x theme (~/.config/awesome/themes/default/theme.lua)
--
-- =============================================================================
local assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local xtheme = xresources.get_current_theme()
local dpi = xresources.apply_dpi

-- Path to layout icons
local layout_path = "/usr/share/awesome/themes/default/layouts/%s.png"

local theme = {}

-- Awesome icon ----------------------------------------------------------------
theme.awesome_icon = assets.awesome_icon(
	dpi(25),
	xtheme.color12,
	xtheme.background
)

-- Border ----------------------------------------------------------------------
theme.border_focus = xtheme.color5
theme.border_normal = xtheme.color0
theme.border_radius = dpi(0)
theme.border_width = dpi(1)

-- Colours ---------------------------------------------------------------------
theme.bg_focus = xtheme.color12
theme.bg_minimize = xtheme.color8
theme.bg_normal = xtheme.background
theme.bg_urgent = xtheme.color9
theme.bg_systray = theme.bg_normal
theme.fg_focus = theme.bg_normal
theme.fg_minimize = theme.bg_normal
theme.fg_normal = xtheme.foreground
theme.fg_urgent = theme.bg_normal

-- Fonts -----------------------------------------------------------------------
theme.font = "Iosevka Nerd Font Mono 10"
-- theme.font_bold = ""
-- theme.font_bold_italic = ""
-- theme.font_icon = ""
-- theme.font_italic = ""
-- theme.font_notification = ""

-- Hotkeys ---------------------------------------------------------------------
-- theme.hotkeys_border_color = ""
-- theme.hotkeys_border_width = dpi(0)
-- theme.hotkeys_fg = ""
-- theme.hotkeys_group_margin = dpi(0)

-- Icon theme ------------------------------------------------------------------
theme.icon_theme = nil

-- Layout ----------------------------------------------------------------------
theme.layout_cornerne = string.format(layout_path, "cornerne")
theme.layout_cornernw = string.format(layout_path, "cornernw")
theme.layout_cornerse = string.format(layout_path, "cornerse")
theme.layout_cornersw = string.format(layout_path, "cornersw")
theme.layout_dwindle = string.format(layout_path, "dwindle")
theme.layout_fairh = string.format(layout_path, "fairh")
theme.layout_fairv = string.format(layout_path, "fairv")
theme.layout_floating = string.format(layout_path, "floating")
theme.layout_fullscreen = string.format(layout_path, "fullscreen")
theme.layout_magnifier = string.format(layout_path, "magnifier")
theme.layout_max = string.format(layout_path, "max")
theme.layout_spiral = string.format(layout_path, "spiral")
theme.layout_tile = string.format(layout_path, "tile")
theme.layout_tilebottom = string.format(layout_path, "tilebottom")
theme.layout_tileleft = string.format(layout_path, "tileleft")
theme.layout_tiletop = string.format(layout_path, "tiletop")
theme = assets.recolor_layout(theme, theme.fg_normal)

-- Menu ------------------------------------------------------------------------
-- theme.menu_height = dpi(25)
-- theme.menu_submenu = "\u{f105}"
-- theme.menu_submenu_icon = theme.icon_theme
-- theme.menu_width = dpi(150)

-- Notification ----------------------------------------------------------------
-- theme.notification_bg = ""
-- theme.notification_border_color = ""
-- theme.notification_border_width = dpi(0)
-- theme.notification_fg = ""
-- theme.notification_height = dpi(0)
-- theme.notification_icon_size = dpi(0)?
-- theme.notification_margin = dpi(0)
-- theme.notification_max_height = dpi(0)?
-- theme.notification_max_width = dpi(0)?
-- theme.notification_opacity = dpi(0)
-- theme.notification_shape = ""
-- theme.notification.spacing = dpi(0)
-- theme.notification_width = dpi(0)

-- Prompt ----------------------------------------------------------------------
-- theme.prompt_bg = ""
-- theme.prompt_fg = ""

-- Snap ------------------------------------------------------------------------
-- theme.snap_bg = ""
-- theme.snap_shape = ""

-- Systray ---------------------------------------------------------------------
-- theme.systray_icon_spacing = ""

-- Taglist ---------------------------------------------------------------------
-- theme.taglist_bg_empty = ""
-- theme.taglist_bg_focus = ""
-- theme.taglist_bg_normal = ""
-- theme.taglist_bg_occupied = ""
-- theme.taglist_bg_urgent = ""
-- theme.taglist_bg_volatile = ""
-- theme.taglist_fg_empty = ""
-- theme.taglist_fg_focus = ""
-- theme.taglist_fg_normal = ""
-- theme.taglist_fg_occupied = ""
-- theme.taglist_fg_urgent = ""
-- theme.taglist_fg_volatile = ""
-- theme.taglist_font = ""

-- Tasklist --------------------------------------------------------------------
-- theme.tasklist_bg_focus = ""
-- theme.tasklist_bg_minimize = ""
-- theme.tasklist_bg_normal = ""
-- theme.tasklist_bg_urgent = ""
theme.tasklist_disable_icon = true
-- theme.tasklist_fg_focus = ""
-- theme.tasklist_fg_minimize = ""
-- theme.tasklist_fg_normal = ""
-- theme.tasklist_fg_urgent = ""
-- theme.tasklist_font_focus = ""
-- theme.tasklist_font_normal = ""
-- theme.tasklist_font_urgent = ""
-- theme.tasklist_plain_task_name = true
-- theme.tasklist_spacing = dpi(0)

-- Titlebar --------------------------------------------------------------------
-- theme.titlebar_bg_focus = ""
-- theme.titlebar_bg_normal = ""
-- theme.titlebar_bg_marked = ""
-- theme.titlebar_fg_focus = ""
-- theme.titlebar_fg_normal = ""
-- theme.titlebar_fg_marked = ""

-- Tooltip ---------------------------------------------------------------------
-- theme.tooltip_bg = ""
-- theme.tooltip_border_color = ""
-- theme.tooltip_border_width = dpi(0)
-- theme.tooltip_fg = ""

-- Useless gap -----------------------------------------------------------------
theme.useless_gap = dpi(4)

-- Wallpaper -------------------------------------------------------------------
theme.wallpaper = "~/Pictures/wallpaper.png"

-- Wibar -----------------------------------------------------------------------
theme.wibar_height = dpi(25)

-- theme.fullscreen_hide_border = ""
-- theme.maximized_hide_border = ""

return theme
