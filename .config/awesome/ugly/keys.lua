-- =============================================================================
--
-- Awesome 4.x keys (~/.config/awesome/ugly/keys.lua)
--
-- =============================================================================
local apply_dpi = require("beautiful.xresources").apply_dpi
local awful = require("awful")
local gears = require("gears")

-- Applications
local browser = os.getenv("BROWSER") or "firefox"
local terminal = os.getenv("TERMINAL") or "xterm"

-- Miscellaneous
local percent = 3
local sink = "alsa_output.pci-0000_05_00.0.analog-stereo"

-- Resize useless gaps with given amount or pixels
function resize_useless_gaps(pixels)
	return function()
		local s = awful.screen.focused()
		s.selected_tag.gap = s.selected_tag.gap + apply_dpi(tonumber(pixels))
		awful.layout.arrange(s)
	end
end

-- Open given web page in browser
local function open_in_browser(web_page)
	return function()
		awful.spawn(("%s %s"):format(browser, web_page))
	end
end

-- Run given command
local function run_command(command)
	return function()
		if command == terminal then ("%s -t %s").format(command, command) end
		awful.spawn(command)
	end
end

-- Run given command in terminal
local function run_in_terminal(command, name)
	return function()
		local title = command:match("%w+")
		awful.spawn(("%s -e '%s' -t '%s' --name '%s'"):format(
			terminal,
			command,
			title,
			name or title
		))
	end
end

return {
	client = gears.table.join(
		-- Client --------------------------------------------------------------
		awful.key({"Mod4", "Control"}, "f", function(c)
			c.fullscreen = not c.fullscreen
			c:raise()
		end),
		awful.key({"Mod4", "Control", "Shift"}, "f", awful.client.floating.toggle),
		awful.key({"Mod4", "Control"}, "m", function(c)
			c.maximized = not c.maximized
			c:raise()
		end),
		awful.key({"Mod4", "Control"}, "n", function(c)
			c:move_to_screen()
		end),
		awful.key({"Mod4", "Control"}, "t", function(c)
			c.ontop = not c.ontop
		end),
		awful.key({"Mod4", "Control"}, "x", function(c)
			c:kill()
		end)
	),
	global = gears.table.join(
		awful.key({"Mod4", "Control"}, "h", function()
			awful.client.swap.byidx(1)
		end),
		awful.key({"Mod4", "Control"}, "l", function()
			awful.client.swap.byidx(-1)
		end),
		awful.key({"Mod4", "Control"}, "u", awful.client.urgent.jumpto),
		-- Launch --------------------------------------------------------------
		awful.key({"Mod4"}, "b", open_in_browser()),
		awful.key({"Mod4"}, "c", run_in_terminal("calcurse")),
		awful.key({"Mod4"}, "e", run_in_terminal("mutt")),
		awful.key({"Mod4", "Shift"}, "e", run_in_terminal("vim")),
		awful.key({"Mod4"}, "f", run_in_terminal("ranger")),
		awful.key({"Mod4", "Shift"}, "f", run_in_terminal("sudo ranger", "ranger")),
		awful.key({"Mod4"}, "i", run_in_terminal("irssi")),
		awful.key({"Mod4"}, "j", run_in_terminal("bash -c neofetch;bash")),
		awful.key({"Mod4"}, "k", run_in_terminal("bc -l")),
		awful.key({"Mod4"}, "m", run_in_terminal("ncmpcpp")),
		awful.key({"Mod4"}, "n", run_in_terminal("newsboat")),
		awful.key({"Mod4"}, "p", run_in_terminal("htop")),
		awful.key({"Mod4"}, "r", run_command("rofi -show combi")),
		awful.key({"Mod4"}, "s", run_in_terminal("sc-im")),
		awful.key({"Mod4"}, "t", run_command(terminal)),
		awful.key({"Mod4", "Shift"}, "t", run_command(("sudo %s"):format(terminal))),
		awful.key({"Mod4"}, "q", run_command('power_menu')),
		-- System --------------------------------------------------------------
		awful.key({"Mod4", "Control"}, "q", awesome.quit),
		awful.key({"Mod4", "Control"}, "r", awesome.restart),
		-- Tag -----------------------------------------------------------------
		awful.key({"Mod4"}, "Left", awful.tag.viewprev),
		awful.key({"Mod4"}, "Right", awful.tag.viewnext),
		awful.key({"Mod4"}, ",", resize_useless_gaps(4)),
		awful.key({"Mod4"}, ".", resize_useless_gaps(-4)),
		awful.key({"Mod4", "Control"}, "j", function()
			awful.tag.incmwfact(-0.02)
		end),
		awful.key({"Mod4", "Control"}, "k", function()
			awful.tag.incmwfact(0.02)
		end),
		-- Various -------------------------------------------------------------
		awful.key({}, "Print", run_command("scrot '%Y%m%d-%H%M%S-$wx$h.png' -e 'mv $f ~/Pictures/dumps/'")),
		awful.key({"Mod4"}, "Print", run_command("scrot -e 'xclip -i $f -t image/png -selection clipboard' -s")),
		awful.key({}, "XF86AudioLowerVolume", run_command(("pactl set-sink-volume %s -%d%%"):format(sink, percent))),
		awful.key({"Mod4"}, "XF86AudioLowerVolume", run_command(("mpc -q volume -%d"):format(percent))),
		awful.key({}, "XF86AudioMedia", run_in_terminal("ncmpcpp")),
		awful.key({}, "XF86AudioMute", run_command(("pactl set-sink-mute %s toggle"):format(sink))),
		awful.key({}, "XF86AudioNext", run_command("mpc -q next")),
		awful.key({}, "XF86AudioPlay", run_command("mpc -q toggle")),
		awful.key({}, "XF86AudioPrev", run_command("mpc -q prev")),
		awful.key({}, "XF86AudioRaiseVolume", run_command(("pactl set-sink-volume %s +%d%%"):format(sink, percent))),
		awful.key({"Mod4"}, "XF86AudioRaiseVolume",  run_command(("mpc -q volume +%d"):format(percent))),
		awful.key({}, "XF86AudioStop", run_command("mpc -q stop")),
		awful.key({}, "XF86Calculator", run_in_terminal("bc -l")),
		awful.key({}, "XF86HomePage", open_in_browser("https://github.com/joakimaling")),
		awful.key({}, "XF86Mail", run_in_terminal("mutt")),
		awful.key({}, "XF86Tools", run_in_terminal("ncmpcpp"))
	)
}
