-- =============================================================================
--
-- Ugly modules for Awesome 4.x (~/.config/awesome/ugly/init.lua)
--
-- =============================================================================

return {
	buttons = require("ugly.buttons"),
	keys = require("ugly.keys"),
	rules = require("ugly.rules"),
	tags = require("ugly.tags")
}
