-- =============================================================================
--
-- Awesome 4.x tags (~/.config/awesome/ugly/tags.lua)
--
-- =============================================================================
local awful = require("awful")

local tags = {}

table.insert(tags, { ------ Web browsers ---------------------------------------
	name = "Web-0",
	properties = {
		name = "\u{f0ac}",
		layout = awful.layout.suit.tile,
		screen = screen[1],
		selected = true
	}
})

-- One more web tag, if more screens
if screen.count() > 1 then
	table.insert(tags, { -- Web browsers ---------------------------------------
		name = "Web-1",
		properties = {
			name = "\u{f0ac}",
			layout = awful.layout.suit.tile.left,
			master_width_factor = 0.7,
			screen = screen[2],
			selected = true
		}
	})
end

-- A terminal tag for each screen
for key = 1, screen.count() do
	table.insert(tags, { -- Terminals ------------------------------------------
		name = string.format("Terminal-%d", key - 1),
		properties = {
			name = "\u{f120}",
			layout = awful.layout.suit.floating,
			screen = screen[key]
		}
	})
end

table.insert(tags, { ------ Development ----------------------------------------
	name = "Code",
	properties = {
		name = "\u{f121}",
		layout = awful.layout.suit.tile,
		screen = screen[1]
	}
})

table.insert(tags, { ------ Calendar & e-mail ----------------------------------
	name = "Office",
	properties = {
		name = "\u{f0b1}",
		layout = awful.layout.suit.tile,
		screen = screen.count() > 1 and screen[3] or screen[1],
		selected = true
	}
})

table.insert(tags, { ------ News feed ------------------------------------------
	name = "Feed",
	properties = {
		name = "\u{f09e}",
		layout = awful.layout.suit.tile,
		screen = screen.count() > 1 and screen[3] or screen[1]
	}
})

table.insert(tags, { ------ Conferencing ---------------------------------------
	name = "Conference",
	properties = {
		name = "\u{f03d}",
		layout = awful.layout.suit.max,
		screen = screen[1]
	}
})

table.insert(tags, { ------ Chat -----------------------------------------------
	name = "Chat",
	properties = {
		name = "\u{f4ad}",
		layout = awful.layout.suit.tile,
		screen = screen.count() > 1 and screen[2] or screen[1]
	}
})

return tags
