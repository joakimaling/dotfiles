-- =============================================================================
--
-- Awesome 4.x rules (~/.config/awesome/ugly/rules.lua)
--
-- =============================================================================
local awful = require("awful")
local beautiful = require("beautiful")

return {
	{ -- Default rules ---------------------------------------------------------
		properties = {
			border_color = beautiful.border_normal,
			border_width = beautiful.border_width,
			buttons = require("ugly.buttons").client,
			focus = awful.client.focus.filter,
			keys = require("ugly.keys").client,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen,
			raise = true,
			screen = awful.screen.preferred
		},
		rule = {}
	},
	{ -- Calculator ------------------------------------------------------------
		properties = {
			floating = true,
			focus = true,
			ontop = true,
			placement = awful.placement.top_left,
			screen = screen.count() > 1 and screen[3] or screen[1],
			skip_taskbar = true
		},
		rule = {
			name = "bc"
		}
	},
	{ -- Calendar & e-mail -----------------------------------------------------
		properties = {
			screen = screen.count() > 1 and screen[3] or screen[1],
			tag = "\u{f0b1}"
		},
		rule_any = {
			class = {"Thunderbird"},
			name = {"calcurse", "mutt"}
		}
	},
	{ -- Chat client -----------------------------------------------------------
		properties = {
			screen = screen.count() > 1 and screen[2] or screen[1],
			tag = "\u{f4ad}"
		},
		rule_any = {
			name = {"Discord", "irssi"}
		}
	},
	{ -- Conferencing ----------------------------------------------------------
		properties = {
			placement = awful.placement.centered,
			tag = "\u{f03d}"
		},
		rule = {
			class = "zoom"
		}
	},
	{ -- Entertainment ---------------------------------------------------------
		properties = {
			callback = function() awful.spawn("mpc -q pause") end,
			new_tag = {
				name = "\u{f11b}",
				layout = awful.layout.suit.max,
				screen = screen[1],
				volatile = true
			},
			placement = awful.placement.centered,
			switch_to_tags = true
		},
		except_any = {
			name = {"Friends List", "Settings", "Steam News*"}
		},
		rule_any = {
			class = {"dosbox", "MPlayer", "Steam", "Wine"},
			name = {"Steam"}
		}
	},
	{
		properties = {
			floating = true,
			ontop = true,
			placement = awful.placement.centered,
			tag = "\u{f11b}"
		},
		rule_any = {
			name = {"Friends List", "Settings", "Steam News*"}
		}
	},
	{ -- Graphics editor -------------------------------------------------------
		properties = {
			new_tag = {
				name = "\u{f03e}",
				layout = awful.layout.suit.tile,
				screen = screen[1],
			},
			switch_to_tags = true
		},
		rule_any = {
			role = {"gimp-image-window-*", "gimp-startup"}
		}
	},
	{
		properties = {
			ontop = true,
			placement = awful.placement.centered,
			screen = screen[1],
			tag = "\u{f03e}"
		},
		except_any = {
			role = {"gimp-image-window-*", "gimp-startup"}
		},
		rule_any = {
			role = {"gimp-*"}
		}
	},
	{ -- Hypervisor ------------------------------------------------------------
		properties = {
			new_tag = {
				name = "\u{f0a0}",
				layout = awful.layout.suit.tile,
				screen = screen.count() > 1 and screen[3] or screen[1],
				volatile = true
			}
		},
		rule = {
			class = "VirtualBox Manager"
		}
	},
	{
		properties = {
			ontop = true,
			tag = "\u{f0a0}"
		},
		rule = {
			class = "VirtualBox Machine"
		}
	},
	{ -- Image viewer ----------------------------------------------------------
		properties = {
			floating = true,
			focus = true,
			ontop = true,
			placement = awful.placement.centered,
			skip_taskbar = true
		},
		rule = {
			class = "feh"
		}
	},
	{ -- Music player ----------------------------------------------------------
		properties = {
			new_tag = {
				name = "\u{f025}",
				layout = awful.layout.suit.max,
				screen = screen.count() > 1 and screen[3] or screen[1],
				volatile = true
			}
		},
		rule = {
			instance = "ncmpcpp"
		}
	},
	{ -- News aggregator -------------------------------------------------------
		properties = {
			screen = screen.count() > 1 and screen[3] or screen[1],
			tag = "\u{f09e}"
		},
		rule = {
			name = "newsboat"
		}
	},
	{ -- Run dialogue ----------------------------------------------------------
		properties = {
			ontop = true,
			skip_taskbar = true
		},
		rule = {
			class = "Rofi"
		}
	},
	{ -- System information ----------------------------------------------------
		properties = {
			below = true,
			border_width = 0,
			focusable = false,
			skip_taskbar = true
		},
		rule = {
			class = "conky"
		}
	},
	{ -- Terminal --------------------------------------------------------------
		properties = {
			size_hints_honor = false
		},
		rule = {
			class = "URxvt"
		}
	},
	{ -- Titlebars -------------------------------------------------------------
		properties = {
			titlebars_enabled = false
		},
		rule_any = {
			type = {"dialog", "normal"}
		}
	},
	{ -- Version control -------------------------------------------------------
		properties = {
			new_tag = {
				name = "\u{f126}",
				layout = awful.layout.suit.max,
				screen = screen.count() > 1 and screen[2] or screen[1],
				volatile = true
			}
		},
		except_any = {
			name = {"Commit*", "Delete*", "*File Compare*", "Revert*"}
		},
		rule = {
			class = "SmartGit"
		}
	},
	{
		properties = {
			placement = awful.placement.centered,
			tag = "\u{f126}"
		},
		rule_any = {
			class = {"SmartGit"},
			name = {"Commit*", "Delete*", "*File Compare*", "Revert*"}
		}
	},
	{ -- Web browsers ----------------------------------------------------------
		properties = {
			screen = 1,
			switch_to_tags = true,
			tag = "\u{f0ac}"
		},
		rule_any = {
			class = {"firefox", "qutebrowser"}
		}
	}
}
