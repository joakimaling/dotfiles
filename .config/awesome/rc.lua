-- =============================================================================
--
-- Awesome 4.x config (~/.config/awesome/rc.lua)
--
-- Requires: vile
--
-- =============================================================================
local awful = require("awful")
local beautiful = require("beautiful")
local filesystem = require("gears.filesystem")
local naughty = require("naughty")
--local vile = require("vile")
local wibox = require("wibox")

require("awful.autofocus")

-- Catch runtime errors --------------------------------------------------------
do
	local in_error = false
	awesome.connect_signal("debug::error", function(message)
		if in_error then return end
		in_error = true

		naughty.notify({
			preset = naughty.config.presets.critical,
			text = tostring(message),
			title = "An error occurred"
		})
		in_error = false
	end)
end

-- Load a theme ----------------------------------------------------------------
beautiful.init(("%sthemes/used/theme.lua"):format(filesystem.get_dir("config")))
local ugly = require("ugly")

-- Used layouts ----------------------------------------------------------------
awful.layout.layouts = {
	awful.layout.suit.floating,
	awful.layout.suit.tile,
	awful.layout.suit.tile.left,
	awful.layout.suit.tile.bottom,
	awful.layout.suit.tile.top,
	awful.layout.suit.fair,
	awful.layout.suit.fair.horizontal,
	awful.layout.suit.max,
	awful.layout.suit.max.fullscreen,
	awful.layout.suit.magnifier,
	awful.layout.suit.corner.nw,
	awful.layout.suit.corner.ne,
	awful.layout.suit.corner.sw,
	awful.layout.suit.corner.se
}

-- Create a keyboard layout widget
my_keyboardlayout = awful.widget.keyboardlayout()

-- Create a text clock widget
my_textclock = wibox.widget.textclock("\u{f017} %d %b %H:%M")

-- Build top panel -------------------------------------------------------------
awful.screen.connect_for_each_screen(function(s)
	-- Create a wibox
	my_wibox = awful.wibar {
		opacity = 0.9,
		position = "top",
		screen = s
	}

	-- Create a layout box
	my_layoutbox = awful.widget.layoutbox(s)
	my_layoutbox:buttons(ugly.buttons.layoutbox)

	-- Populate wibox with widgets
	my_wibox:setup {
		layout = wibox.layout.align.horizontal,
		{
			layout = wibox.layout.fixed.horizontal,
			awful.widget.launcher({
				command = ("%s/scripts/power-menu"):format(os.getenv("HOME")),
				image = beautiful.awesome_icon
			}),
			awful.widget.taglist(s, awful.widget.taglist.filter.all,
				ugly.buttons.taglist)
		},
		awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags,
			ugly.buttons.tasklist),
		{
			layout = wibox.layout.fixed.horizontal,
			my_keyboardlayout,
			my_textclock,
			my_layoutbox
		}
	}
end)

-- Create permanent tags  ------------------------------------------------------
for _, item in ipairs(ugly.tags) do
	awful.tag.add(item.name, item.properties)
end

-- Client rules ----------------------------------------------------------------
awful.rules.rules = ugly.rules

-- Global keys -----------------------------------------------------------------
root.keys(ugly.keys.global)

-- Signals ---------------------------------------------------------------------
client.connect_signal("manage", function(c)
	awful.client.setslave(c)
	if awesome.startup
		and not c.size_hints.user_position
		and not c.size_hints.program_position then
		awful.placement.no_offscreen(c)
	end
end)

-- Let focus follow mouse
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

-- Draw border around focused client
client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
end)

-- Remove border around unfocused client
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
end)

-- Autostart -------------------------------------------------------------------

-- Set wallpaper
awful.spawn.with_shell("bash ~/.fehbg");
