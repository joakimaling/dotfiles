#       _           __    _        __  ___
#      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
#     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
#  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
# |___/    https://gitlab.com/joakimaling    /___/
#
# ~/.config/qutebrowser/config.py
#

# Load in-browser settings
config.load_autoconfig()

# Accept British English first
c.content.headers.accept_language = 'en-GB,en;q=0.9'

# Set download path
c.downloads.location.directory = '~/Downloads'

# Don't ask where to download
c.downloads.location.prompt = False

# Remove finished downloads
c.downloads.remove_finished = 30000

# Close window when last tab is closed
c.tabs.last_close = 'close'

# Move to last used tab when closing focused tab
c.tabs.select_on_remove = 'last-used'

# Nice title format
c.tabs.title.format = '{audio} {current_title}'

# Use custom default page
c.url.default_page = '~/.config/startpage.html'

# Some useful search engine shortcuts
c.url.searchengines = {
	'DEFAULT': 'https://duckduckgo.com/?q={}',
	'aw': 'https://wiki.archlinux.org/?search={}',
	'gh': 'https://github.com/search?q={}',
	'yt': 'https://www.youtube.com/results?search={}'
}

# Fill username and password
config.bind('<z><l>', 'spawn --userscript qute-pass')

# Use imported theme
config.source('nord.py')
