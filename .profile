#       _           __    _        __  ___
#      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
#     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
#  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
# |___/    https://gitlab.com/joakimaling    /___/
#
# ~/.profile
#

# Default directories
[ -z "$XDG_CACHE_HOME" ] && export XDG_CACHE_HOME=~/.cache
[ -z "$XDG_CONFIG_HOME" ] && export XDG_CONFIG_HOME=~/.config
[ -z "$XDG_DATA_HOME" ] && export XDG_DATA_HOME=~/.local/share

# Environment variables
export AUR_PAGER=vim
export BROWSER=qutebrowser
export CALCULATOR=bc
export CALENDAR=calcurse
export EDITOR=vim
export EMAIL_CLIENT=mutt
export FILER=ranger
export GPG_TTY=$(tty)
export HIGHLIGHT_TABWIDTH=2
export LESSOPEN="| less-filter %s"
export LESS=-RX
export MUSIC_PLAYER=ncmpcpp
export PAGER=less
export READER=mupdf
export TERMINAL=termite
export VISUAL=$EDITOR

# Import custom exports
[ -r ~/.exports ] && . ~/.exports

# Add custom binaries
[ -d $XDG_DATA_HOME/Work/scripts ] && export PATH=$XDG_DATA_HOME/Work/scripts:$PATH
[ -d $XDG_DATA_HOME/scripts ] && export PATH=$XDG_DATA_HOME/scripts:$PATH

# Load .bashrc
[ -r ~/.bashrc ] && . ~/.bashrc

# Run window manager upon login
if [ $(tty) = /dev/tty1 ] && [ -x /usr/bin/Xorg ]; then
  startx && logout
fi
