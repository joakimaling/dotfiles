"       _           __    _        __  ___
"      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
"     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
"  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
" |___/    https://gitlab.com/joakimaling    /___/
"
" ~/.vimrc
"

" -- Bundle imports ------------------------------------------------------------

call plug#begin('~/.vim/plugged')
Plug 'ap/vim-css-color'
Plug 'arcticicestudio/nord-vim'
Plug 'chr4/nginx.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'nikvdp/ejs-syntax'
Plug 'isRuslan/vim-es6'
Plug 'jwalton512/vim-blade'
Plug 'lervag/vimtex'
Plug 'mattn/emmet-vim'
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'suan/vim-instant-markdown', {'rtp': 'after'}
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'tpope/vim-commentary'
Plug 'vim-airline/vim-airline'
call plug#end()

" -- Colours & highlighting ----------------------------------------------------

colorscheme nord
hi Normal guibg=NONE ctermbg=NONE

" Colouring odd files
autocmd BufNewFile,BufRead .babelrc set syntax=json
autocmd BufNewFile,BufRead .jshintrc set syntax=json
autocmd BufNewFile,BufRead .postcssrc set syntax=json

" -- General configuration -----------------------------------------------------

set autoread
set cursorline
set encoding=utf-8
set history=1000
set ignorecase
set lazyredraw
set mouse=a
set nocompatible
set number
set relativenumber
set scrolloff=8
set showcmd
set splitbelow
set splitright
set title

" Directories
set backupdir=.,/tmp//
set directory=.,/tmp//
set undodir=/tmp
set viminfo+=n~/.vim/.viminfo

" Indentation
set autoindent
set smartindent
set softtabstop=4
set tabstop=4

" Searches
set hlsearch
set incsearch
set path+=**
set smartcase
set wildignore+=*.bmp,*.gif,*.jpg,*.png,*.class,*.o,*.ppu,.git
set wildignore+=*.aux,*.bbl,*.blg,*.fdb_latexmk,*.fls,*.log,*.synctex.gz,*.toc
set wildmenu

let mapleader=","

" -- Key bindings --------------------------------------------------------------

" Save file in normal/insert mode
imap <F3> <ESC>:w<CR>i
nmap <F3> :w<CR>

" Create source code class/module
map <F4> :!create-class -p "%:p:h" -x %:e<CR>

" Toggle between relative & regular line numbers
map <F6> :set invrelativenumber<CR>

" Open/close NERDTree
map <F9> :NERDTreeToggle<CR>

" Spell checking
map <leader>le :setlocal spell! spelllang=en_gb<CR>
map <leader>ls :setlocal spell! spelllang=sv_se<CR>

map <Leader>md :InstantMarkdownPreview<CR>
map <Leader>ms :InstantMarkdownStop<CR>

" -- Plug-in configuration -----------------------------------------------------

let g:airline_powerline_fonts = 1
let g:instant_markdown_autostart = 0
let g:instant_markdown_browser = 'qutebrowser'
let g:NERDTreeShowHidden = 1
let g:NERDTreeWinSize = 20
let g:pascal_delphi = 0
let g:pascal_fpc = 1
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'mupdf'

" -- Scripts -------------------------------------------------------------------

autocmd VimLeave *.tex !clean-tex %
