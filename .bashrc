#       _           __    _        __  ___
#      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
#     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
#  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
# |___/    https://gitlab.com/joakimaling    /___/
#
# ~/.bashrc
#

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Import custom colours
[ -r ~/.dircolors ] && eval $(dircolors -b ~/.dircolors)

# Import custom aliases
[ -r $XDG_DATA_HOME/scripts/aliases ] && . $XDG_DATA_HOME/scripts/aliases

# Insult the user
[ -r $XDG_DATA_HOME/scripts/insults ] && . $XDG_DATA_HOME/scripts/insults

# Neat settings for history
export HISTCONTROL=erasedups:ignoreboth
export HISTIGNORE="cd:cd *:exit:pwd"
export HISTTIMEFORMAT="[%d %b %H:%M] "
export HISTFILESIZE=65536
export HISTSIZE=4096

# Autocorrect typos in paths
shopt -s cdspell

# Redraw content upon window resize
shopt -s checkwinsize

# Save multi-line commands as one
shopt -s cmdhist

# Expand aliases
shopt -s expand_aliases

# Append to the history file
shopt -s histappend

# Update custom prompt
update_prompt() {
	PS1=$($XDG_DATA_HOME/scripts/powerline/ps1.py $?)
}

PROMPT_COMMAND=update_prompt

# Use vim mode
set -o vi
