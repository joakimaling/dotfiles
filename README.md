# dotfiles

> A collection of my personal scripts and configuration files

[![Licence][licence-badge]][licence-url]

This project contains various scripts and configuration files for software I use
daily. It's also a component within the [Architect][architect-url] project.

## Contents

This project contains configuration files for the following software:

[awesome](https://www.archlinux.org/packages/community/x86_64/awesome/),
[calcurse](https://www.archlinux.org/packages/community/x86_64/calcurse/),
[conky](https://www.archlinux.org/packages/extra/x86_64/conky/),
[ctags](https://archlinux.org/packages/extra/x86_64/ctags/),
[cups](https://archlinux.org/packages/extra/x86_64/cups/),
[curl](https://www.archlinux.org/packages/core/x86_64/curl/),
[dosbox](https://www.archlinux.org/packages/community/x86_64/dosbox/),
[dunst](https://archlinux.org/packages/community/x86_64/dunst/),
[fcitx5](https://archlinux.org/packages/community/x86_64/fcitx5/),
[feh](https://www.archlinux.org/packages/extra/x86_64/feh/),
[git](https://www.archlinux.org/packages/extra/x86_64/git/),
[gnupg](https://archlinux.org/packages/core/x86_64/gnupg/),
[htop](https://www.archlinux.org/packages/extra/x86_64/htop/),
[i3-wm](https://archlinux.org/packages/community/x86_64/i3-wm/),
[irssi](https://archlinux.org/packages/extra/x86_64/irssi/),
[mariadb](https://archlinux.org/packages/extra/x86_64/mariadb/),
[mpd](https://www.archlinux.org/packages/extra/x86_64/mpd/),
[mplayer](https://www.archlinux.org/packages/extra/x86_64/mplayer/),
[mutt](https://www.archlinux.org/packages/extra/x86_64/mutt/),
[ncmpcpp](https://www.archlinux.org/packages/community/x86_64/ncmpcpp/),
[neofetch](https://aur.archlinux.org/packages/neofetch/),
[newsboat](https://www.archlinux.org/packages/community/x86_64/newsboat/),
[npm](https://archlinux.org/packages/community/any/npm/),
[opendoas](https://archlinux.org/packages/community/x86_64/opendoas/),
[pulseaudio](https://archlinux.org/packages/extra/x86_64/pulseaudio/),
[ranger](https://www.archlinux.org/packages/community/any/ranger/),
[rofi](https://www.archlinux.org/packages/community/x86_64/rofi/),
[sxiv](https://archlinux.org/packages/community/x86_64/sxiv/),
[termite](https://www.archlinux.org/packages/community/x86_64/termite/),
[tmux](https://www.archlinux.org/packages/community/x86_64/tmux/),
[transmission-cli](https://archlinux.org/packages/extra/x86_64/transmission-cli/),
[umlet](https://aur.archlinux.org/packages/umlet/),
[vim](https://www.archlinux.org/packages/extra/x86_64/vim/),
[virtualbox](https://archlinux.org/packages/community/x86_64/virtualbox/),
[wget](https://www.archlinux.org/packages/extra/x86_64/wget/),
[xorg-xinit](https://www.archlinux.org/packages/extra/x86_64/xorg-xinit/)

## Installation

:warning: Only run `install.sh` if you know what you're doing, or you've backed
up your home directory, since the script will overwrite some of these files!

```sh
git clone --recursive https://gitlab.com/carolinealing/dotfiles.git
./dotfiles/install.sh
```

## Licence

Released under GNU-3.0. See [LICENSE][licence-url] for more.

Coded with :heart: by [carolinealing][user-url].

[architect-url]: http://gitlab.com/carolinealing/architect
[licence-badge]: https://badgen.net/gitlab/license/carolinealing/dotfiles
[licence-url]: LICENSE
[user-url]: https://gitlab.com/carolinealing
